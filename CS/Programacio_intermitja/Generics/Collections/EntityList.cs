﻿using Generics.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Generics.Collections
{
    // això seria un REPOSITORI

    public class EntityList<T> : GenericList<T>, IEntityList<T> where T : Entity, new()
        // hereta de GenericList + restricció= ha de ser IEntityList!!!
    {
        public override void Add(T item)
        {
            //per exemple, comprovar permisos d'Usuari!!

            base.Add(item);
        }
    }
}
