﻿using Generics.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Generics.Collections
{
    //només per objectes Entity (+ derivats)
    public interface IEntityList<T> : IGenericList<T> where T : Entity, new()
    //Hereta de GenericList, per poder usar: Add, Count, getNext!!!
    // + restricció amb el where = tipus Entity (fins ara el T agafava qualsevol cosa!!)
    {
        //Creant la col.lecció

    }
}
 