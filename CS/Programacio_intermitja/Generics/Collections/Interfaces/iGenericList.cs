﻿
namespace Generics.Collections
{
    //INTERFACE

    public interface IGenericList<T> : IGenericList
    // interface generic que hereta d'una no genèrica
    {
     
        void Add(T item);
    }

    // interface NO generic
    public interface IGenericList
    {
        int Count { get; set; }
        object GetNext();
    }
}
