﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Generics.Collections
{
    public class GenericList : IGenericList
    /// 
    {
        protected object[] _items;
        // objecte més genèric possible = object (array)

        private int _currentIndex = 0;

        //ens cal aquesta property
        public int Count { get; set; }

        public GenericList()
        {
            //tamany de 4
            _items = new object[4];
        }

        protected void AddObject(object item)
        {
            if (Count > _items.Length)
            {
                //si passa, cal redimensionar l'array
                var newArray = new object[_items.Length + 4];

                for (var i = 0; i < _items.Length; i++)
                {
                    newArray[i] = _items[i];
                }
                _items = newArray;
            }
            _items[Count] = item;
            Count++;
        }

        // element següent
        public object GetNext()
        {
            var output = _items[_currentIndex];
            if (_currentIndex++ == _items.Length)
                _currentIndex = 0;

            return output;
        }
    }

    public class GenericList<T> : GenericList, IGenericList<T>
    /// la lletra és aleatoria!
    {
        //afegim una property tipus T d'array!!
        public T[] Items 
        //l'emmascarem
        {
            get 
            {
                // anul.lat: Lenght dóna molts NUlls per que compta espais
                //var outout = new T[_items.Length];

                // Count = opera amb reales (no nulls)
                var output = new T[Count];

                for(var i = 0; i < Count; i++ )
                {
                    output[i] = (T)_items[i];
                }
                return output;
            }
            
        }

        //el fem virtual per poder-lo extendre, pel tipus T only
        public virtual void Add(T item)
        {
            //hereta
            base.AddObject(item);

            // cal una constrain (RESTRICCIO)  a la classe general => where...
            //T a = new T();
        }
    }
}
