﻿using Generics.Models;


namespace Generics.Models
{
    public class User : Entity
    {
        public string Name {  get; set; }

        //donam un tipus T, on T és un User
        public void SayHello<T>(T User) where T : User
        {

        }
        //metodes genèrics
        public void SayHello(User user)
        {

        }

        public User()
        {

        }
    }
}
