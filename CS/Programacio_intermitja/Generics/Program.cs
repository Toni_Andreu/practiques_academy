﻿using Generics.Collections;
using Generics.DAL;
using Generics.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Generics
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");

        /// 1- Creem una generic list
            /// 
            //var genericlistInt = new GenericList<int>();
            IGenericList<int> genericlistInt = new GenericList<int>();

            genericlistInt.Add(1);
            genericlistInt.Add(4);
            genericlistInt.Add(8);
            genericlistInt.Add(5);
            genericlistInt.Add(9);

            var size = genericlistInt.Count;

            // altres possibilitats
            var genericlistString = new GenericList<string>();
            //genericlistString.Add(1);
            //genericlistString.Add(4);
            //genericlistString.Add(8);
            //genericlistString.Add(5);
            //genericlistString.Add(9);
            //var size2 = genericlistString.Count;

        /// 2- Després l'adaptem a l'interface generic
            IGenericList<double> genericlistDouble = new GenericList<double>();
            genericlistDouble.Add(5);

            //array de llistes genèriques
            //var listasGenericas = new iGenericList[4];
            //listasGenericas[0] = genericlistInt;
            //listasGenericas[1] = genericlistString;
            //listasGenericas[2] = genericlistDouble;

            //llista genèric, sense array
            var listasGenericas = new GenericList<IGenericList>();
            listasGenericas.Add(genericlistInt);
            listasGenericas.Add(genericlistString);
            listasGenericas.Add(genericlistDouble);
            

            //el conectem al mètode per Print
            PrintNextElement(listasGenericas);

            /// 3- l'afegim una restriccció de tipus i creem un repositori

            var repoEntities = new EntityList<Entity>();
            // crear un llistat d'usuaris
            var repoUsers = new UsersList<User>();

            var student = new Student();
            repoUsers.Add(student);

            var subject = new Subject();
            //repoUsers.Add(subject); // dóna error

            repoEntities.Add(student);
            repoEntities.Add(subject);

            /// 4- li donem una property generica
            // només veiem tipus Users
            //repoUsers.Items

            var primerItem = repoEntities.Items[0];

            /// 5- Fem un mètode genèric
            
            var company = new Student();    
            student.SayHello<Student>(company);


        }
        // Usem un generic list instead
        public static void PrintNextElement(IGenericList listas)
        // Anul.lat: array llistes genèriques
        //public static void PrintNextElement(iGenericList[] listas)
        {
            for(var i = 0; i < listas.Count; i++)
            {
                //obtenim la llista
                var lista = (IGenericList)listas.GetNext();
                //capturem l'element
                var element = lista.GetNext;

                Console.WriteLine(element.ToString());

            }
        }
    }
}
