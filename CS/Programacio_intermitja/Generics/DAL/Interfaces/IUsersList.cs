﻿using Generics.Models;
using Generics.Collections;

namespace Generics.DAL
{
    // restricció = only pels Users!!
    public interface IUsersList<Tuser> : IEntityList<Tuser> where Tuser : User, new()
    {
    }
}
