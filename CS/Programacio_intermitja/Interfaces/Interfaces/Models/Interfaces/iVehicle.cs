﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

//namespace Interfaces.Models.Interfaces
namespace Interfaces.Models
{
    public interface iVehicle : iPurchasable
    {
        //Definim les propietats que ha tenir un vehicle:

        //MATRICULA
        public string LicenseId {  get; set; }   
        //
        public double NormalSpeed { get; set; }
        
        bool StartEngine();
        bool MoveForward();
    }
}
