﻿
using System;
using System.Collections.Generic;

namespace Interfaces.Models
{
    public class TrafficLight
    {
        // llista interna => private
        //public List<iVehicle> CurrentVehicles { get; private set; } = new Dictionary<iVehicle>();
        // modifiquem a Dictionary!!
        public Dictionary<string, iVehicle> CurrentVehicles { get; private set; } = new Dictionary<string, iVehicle>();

        //s'estan bellugant ???
        public bool IsMoving { get; private set; }

        public TrafficLight()
        {

        }
        // anul.lem llista i usarem el Dictionary
        //public void StartTraffic(List<iVehicle> vehicles)
        public void StartTraffic()
        {
            //loop que captura el diccionary
            foreach (var vehicle in CurrentVehicles.Values)
            {
                vehicle.StartEngine();
                vehicle.MoveForward();
            }
        }

        //amb el set privat
        //creem un métode per afegir-ne
        public void AddVehicle(iVehicle vehicle)
            //poliformisme: pot heredar de qualsevol interface!!
        {
            if (IsMoving)
                throw new InvalidOperationException("No es pot afegir un vehicla a la circulació amb el semàfor en vermell!");

            if (CurrentVehicles.ContainsKey(vehicle.LicenseId))
                throw new ArgumentException("El vehicle ja està a la carretera!");

            //ara és Dictionary!!
            CurrentVehicles.Add(vehicle.LicenseId, vehicle);
        }


    }
}
