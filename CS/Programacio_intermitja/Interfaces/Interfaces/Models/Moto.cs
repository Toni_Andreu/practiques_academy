﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Interfaces.Models
{
    public class Moto : iVehicle, iTaxable
    {
        public string LicenseId { get; set; }

        public double NormalSpeed { get; set; } = 20;
        //per defecte = 20 kmH


        public double DetermineCurrentMarketValue()
        {
            return 10000.0;
        }

        public bool MoveForward()
        {
            Console.WriteLine($"La moto es belluga a {NormalSpeed}");
            return true;
        }

        public bool StartEngine()
        {
            Console.WriteLine("La moto arrenca!!");
            return true;
        }


    }
}