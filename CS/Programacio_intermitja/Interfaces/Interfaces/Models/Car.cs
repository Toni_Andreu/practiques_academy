﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Interfaces.Models
{
    // declara TIPUS, no herencia!!!
    public class Car : iPurchasable, iVehicle, iTaxable
    {
        public double NormalSpeed { get; set; } = 30;
        //per defecte = 30 kmH

        //MATRICULA
        public string LicenseId { get; set; }

        public double DetermineCurrentMarketValue()
        {
            return 10000.0;
        }

        public bool MoveForward()
        {
            Console.WriteLine($"El cotxe es belluga a {NormalSpeed}");
            return true;
        }

        public bool StartEngine()
        {
            Console.WriteLine("El cotxe arrenca!!");
            return true;
        }
    }


}
