﻿using Interfaces.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Interfaces
{
     class Program
    {
        public static TrafficLight CurrentTrafficLight { get; set; }


        static void Main(string[] args)
        {
            Console.WriteLine("HelloWorld!!!");

            var car = new Car()
            {
                LicenseId = "3216DHD"
            };

            var moto = new Moto()
            {
                LicenseId = "1234DHD"
            };

            //màscares del matexi objecte!
            // Per veure'l des de vehicle, taxable, purchasable!!
            //iVehicle vehicle1 = car;

            //ANU.LLAT amb la classe TrafficLight
            //pot arrencar?
            //var canStart = vehicle1.StartEngine();
   

            //iTaxable taxable = car;
            //// quant costa?
            //var howMuch = vehicle1.DetermineCurrentMarketValue();

            /// Anu.lat després de Dictionary en TrafficLights!!
            //var vehiclesAtTheMoment = new List<iVehicle>();
            //vehiclesAtTheMoment.Add(vehicle1);

            //StartTraffic(vehiclesAtTheMoment);

            //iPurchasable item = car;

            CurrentTrafficLight.AddVehicle(car);
            CurrentTrafficLight.AddVehicle(moto);

            CurrentTrafficLight.StartTraffic();


       
        //funcio "read only"
        //public static void StartTraffic(List<iVehicle> vehicles)
        //{
        //    //loop
        //    foreach(var vehicle in vehicles)
        //    {
        //        vehicle.StartEngine();
        //        vehicle.MoveForward();
        //    }
        }

    }
}
