﻿// See https://aka.ms/new-console-template for more information

using System.Collections;

Console.WriteLine("Hello, World!");

// forma de crear una mena de scope que es destrueix quan acaba
using (var enumerator = new GenericEnumerable().GetEnumerator())
{
    Console.WriteLine("Hello World!");

    static void Main()
    { 
        var miEnumerable = new GenericEnumerable();
        var currentEnumerator = miEnumerable.GetEnumerator();

        while (currentEnumerator.MoveNext())
        {
            Console.WriteLine(currentEnumerator.Current.ToString());
        }
        foreach (var element in miEnumerable)
        {
            Console.WriteLine(element.ToString());
        }
    }
}


class MyEnumerator : IEnumerator, IDisposable
{
    public int Current {  get; set; }

    object IEnumerator.Current => throw new NotImplementedException();

    public bool Dispose()
    {
        throw new NotImplementedException();
    }

    public bool MoveNext()
    {
        throw new NotImplementedException();
    }

    public void Reset()
    {
        throw new NotImplementedException();
    }

    void IDisposable.Dispose()
    {
        throw new NotImplementedException();
    }
}

//Creo una mena de  "genericList"
class GenericEnumerable : IEnumerable
{
    protected MyEnumerator MyEnumerator = new MyEnumerator();

    public IEnumerator GetEnumerator()
    {
        throw new NotImplementedException();
    }
}
