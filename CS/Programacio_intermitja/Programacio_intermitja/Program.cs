﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Programacio_intermitja.Models;

namespace Programacio_intermitja
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World"); 


            //prohivit per que Person ara és ABSTRACT
            //var person = new Person();


            //prohivit per que Person ara és ABSTRACT
            //var user = new User();

            // = cal definir el tipus !!!
            //var currante = new Customer();

            //Polimorfismo

            //variable tipus Employee
            Employee employee = new Employee();

            //el convertim en Persona i en User
            var person = employee as Person;
            var user = employee as User;
            var customer = person as customer; 

            employee.TellTo("Com estàs?", "Pep");
            user.TellTo("Com estàs?", "Pep");
            person.TellTo("Com estàs?", "Pep");
            customer.TellTo("Com estàs?", "Pep");

            var v1 = new Vector2(5, 2);
            var v2 = new Vector2(1, 8);

            var v3 = v1 + v2; //no deixa! Cal un OVERLOAD a Vector2!!!!!
        }
    }
}
