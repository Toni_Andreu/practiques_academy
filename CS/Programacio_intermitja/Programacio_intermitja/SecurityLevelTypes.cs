﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Programacio_intermitja
{
    public enum SecurityLevelTypes
    {
        None,
        Basic,
        SysAdmin,
        Customer,
        Worker
    }
}
