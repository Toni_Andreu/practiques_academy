﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Programacio_intermitja
{
    var point = new Point(4, 6);
    struct Employee
    {
        public double Salary { get; set; }
        public Employee(double salary)
        {
            Salary = salary;
        }
    }

    // típico struct per coordenades
    struct Point
    {
        public int x;
        public int y;

        public Point(int _x, int _y)
        {
            x = _x;
            y = _y;
        }

        public void Print()
        {
            Console.WriteLine($" Aqui teniu els punts: {x},{y}");
        }
    }
}
