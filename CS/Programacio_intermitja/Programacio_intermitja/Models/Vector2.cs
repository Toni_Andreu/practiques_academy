﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Programacio_intermitja.Models
{
    public class Vector2  
    {
        public double X {  get; set; }
        public double Y {  get; set; }

        public double Magnitud {  get; set; }
        public Vector2(double x, double y)
        {
            X = x;
            Y = y;
        }

        //overload d'operadors
        public static Vector2 operator + (Vector2 v1, Vector2 v2)
        {
            var output = new Vector2(v1.X + v2.X, v1.Y + v2.Y);
            output.Magnitud = v1.Magnitud + v2.Magnitud;
            return output;
        }
        public static Vector2 operator - (Vector2 v1, Vector2 v2)
        {
            var output = new Vector2(v1.X + v2.X, v1.Y + v2.Y);
            output.Magnitud = v1.Magnitud + v2.Magnitud;
            return output;
        }
    }
}
