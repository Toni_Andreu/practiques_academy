﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Programacio_intermitja.Models
{
    public abstract class User : Person
    {
        public SecurityLevelTypes securityLevelType { get; set; }

        public User()
            //forma de dir-li que CREII la classe a Person
            : base("")
        {

        }
        public User(string dni)
            : this()
        {

        }
        //public User(string dni)
        //    : base(dni: "37742582L")
        //{
        //}

        ///override
        public override void TellTo(string message, string to)
        {
            throw new NotImplementedException();
            Console.WriteLine($"Hola {to} et volia dir que: {message}");
        }
        public virtual void LogMessage( string msg, SecurityLevelTypes authlevel)
        {
            //do sth
        }
    }
}


