﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Programacio_intermitja.Models
{
    public class Employee : User
    {
        public override void TellTo(string message, string to)
        {
            throw new NotImplementedException();
            // per mantenir el principi "sòlid" de programació
            // = passar pel la classe mare primer!
            base.TellTo(message, to);
            // més l'extensió d'Employee = 2n missatge!
            //Extenem la classe sense saltar-nos la mare!
            Console.WriteLine($"Benvingut {to}");
        }
    }
}
