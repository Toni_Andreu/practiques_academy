﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Programacio_intermitja.Models
{
    public abstract class Person
    {
        public string Dni {  get; set; }
        public string Telf { get; set; }

        public string Name { get; set; }

        public string Surname { get; set; }

        public Person()
        {

        }
        public Person(string dni)
        {

        }
        public Person(string dni, string telf)
        {

        }

        //public void TellTo(string message, string to)
        //{
        //    Console.WriteLine($"Hola {to} et volia dir que: {message}");
        //}

        public abstract void TellTo(string message, string to);
       
            
     
    }
}
