﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SubtitlesTranslator.App.WPF.Lib.Models
{
    public class SubtitleLine
    {
        #region statics

        public static List<SubtitleLine> GetTextFromLines(List<string> lines)
        {
            var output = new List<SubtitleLine>();

            for(var i=2; i< lines.Count-4; i++)
            {
                if( int.TryParse(lines[i], out int lineNumber))
                {
                    var subtitleline = new SubtitleLine()
                    {
                        LineNumber = lineNumber,
                        Period = lines[i + 1],
                        Text = lines[i + 2] + " " + lines[i + 3]
                    };

                    output.Add(subtitleline);
                    i += 4;
                }
                else
                {
                    throw new ArgumentException($"Els subtítols no estan bé, la línia:{i} [{lines[i]}] havia de ser un nombre enter ");
                }
            }

            return output;
        }

        #endregion
        public int LineNumber { get; set; }

        public string Period { get; set; }

        public string Text { get; set; }
    }
}
