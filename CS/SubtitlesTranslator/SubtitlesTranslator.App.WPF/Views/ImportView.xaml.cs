﻿using Microsoft.Win32;
using SubtitlesTranslator.App.WPF.ViewModels;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows.Controls;


namespace SubtitlesTranslator.App.WPF.Views
{
    /// <summary>
    /// Interaction logic for ImportView.xaml
    /// </summary>
    public partial class ImportView : UserControl
    {
        public ImportViewModel ViewModel { get; set; }

        public ImportView()
        {
            InitializeComponent();

            ViewModel = new ImportViewModel();
            //injecció del GetSubtitlesFiles
            ViewModel.GetSubtitlesFilesAction = GetSubtitlesFiles;
            //metode anònim per llegir les línees
            ViewModel.GetFileTextLinesAction = (path) =>
            {
                if (!string.IsNullOrEmpty(path))
                    return File.ReadAllLines(path).ToList();
                return new List<string>();
            };

            DataContext = ViewModel;
        }

        //funcionalitat per decidir quins fitxers volem pujar
        // result Llistat de paths
        private List<string> GetSubtitlesFiles()
        {
            var output = new List<string>();

            var ofd = new OpenFileDialog();
            ofd.Multiselect = true;
            //only this kind of files!
            ofd.Filter = "vtt subtitle files (*.vtt)|*vtt";

            ofd.ShowDialog();
            output.AddRange(ofd.FileNames);

            return output;
        }
 
    }
}
