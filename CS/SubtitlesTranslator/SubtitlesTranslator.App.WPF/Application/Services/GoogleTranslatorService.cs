﻿using Google.Api.Gax.ResourceNames;
using Google.Cloud.Translation.V2;
using Newtonsoft.Json;
using SubtitlesTranslator.App.WPF.Lib;
using SubtitlesTranslator.App.WPF.Lib.Models;
using SubtitlesTranslator.Lib.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Net;
using System.Timers;
using System.Web;
using System.Windows;

namespace SubtitlesTranslator.App.WPF.ApplicationServices
{
    public class GoogleTranslatorService : ITranslatorService
    {
        private string LanguageFrom { get; set; }
        private string LanguageTo { get; set; }

        // com un array
        private SubtitleLine[] Input { get; set; }
     
        private List<SubtitleLine> Preoutput { get; set; }
        private ObservableCollection<SubtitleLine> Output { get; set; }

        //timer
        private static Timer aTimer;

        private int Count{get; set; } = 0;

        public GoogleTranslatorService()
        {
        }

        public void Translate(string from, string to, IEnumerable<SubtitleLine> input, ObservableCollection<SubtitleLine> output)
        {
            LanguageFrom = from;
            LanguageTo = to;
            Input = input.ToArray();
            Output = output;

            //Input en paquets màxims de 50
            var translatedTexts = new List<string>();
            var inputs = Input.Select(x => x.Text);
            var total = inputs.Count();

            if (total > 50)
            {
                var page = 0;

                while(translatedTexts.Count < total)
                {
                    var section = inputs.Skip(page * 50).Take(50).ToArray();
                    translatedTexts.AddRange(Translate(LanguageFrom, LanguageTo, section));
                    page++;
                }
            }
            else
            {
                translatedTexts.AddRange(Translate(LanguageFrom, LanguageTo, inputs.ToArray()));
            }
            

            Preoutput = new List<SubtitleLine>();

            //Traductor línea a Línea!
            for (var i = 0; i < Input.Length; i++)
            {
                Preoutput.Add(new SubtitleLine()
                {
                    LineNumber = Input[i].LineNumber,
                    Period = Input[i].Period,
                    Text = translatedTexts[i]
                });
            }
            //Timer miliseconds
            aTimer = new Timer(50);
            //Hook up the Elapsed event for the timer.
            aTimer.Elapsed += OnTimedEvent;
            aTimer.AutoReset = true;
            aTimer.Enabled = true;
        }

        public List<string> Translate(string fromLanguage, string toLanguage, string[] inputs)
        {
            var from = LanguageOptions.Types[fromLanguage];
            var to = LanguageOptions.Types[toLanguage];

            //var jsonKeyFile = File.ReadAllText("Key.json");
            //var jsonKey = JsonConvert.DeserializeObject<JsonKey>(jsonKeyFile);

            //var key = "Pon Aquí tu Clave!";
            //var key = jsonKey.Key;

            var key = "AIzaSyAvJBkNnSvc - V7fp6PPcFRj7uy9kC6fcPw";
            var url = $"https://translate.googleapis.com/language/translate/v2?key={key}&sl={from}&target={to}";

            foreach (var input in inputs)
            {
                url += $"&q={HttpUtility.UrlEncode(input)}";
            }
            
            var webClient = new WebClient
            {
                Encoding = System.Text.Encoding.UTF8,
            };
            
            try
            {
                var result = webClient.DownloadString(url);

                //convertim el Json
                var trs = JsonConvert.DeserializeObject<Root>(result);
                return trs.data.translations.Select(x => x.translatedText).ToList();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return null;
            }

        }

        private void OnTimedEvent(object? sender, ElapsedEventArgs e)
        {
            var line = Input[Count];
            
            //per cada linea, traduction service
            Application.Current.Dispatcher.Invoke(new Action(() =>
            {
                Output.Add(Preoutput[Count]);
            }));
            Count ++;

            //atura el Timer
            if(Count == Input.Length)
            {
                aTimer.Stop();
                aTimer.Dispose();

                Application.Current.Dispatcher.Invoke(new Action(() =>
                {
                    MessageBox.Show($"Traducció completada. Total de línees:¨{Count}");
                    // deixa a 0 l'anterior
                    Count = 0;
                }));
            }
        }

       

        //public string Translate222(string fromLanguage, string toLanguage, string input)
        //{
        //    var from = LanguageOptions.Types[fromLanguage];
        //    var to = LanguageOptions.Types[toLanguage];
        //    var url = $"https://translate.googleapis.com/translate_a/single?client=gtx&sl={from}&tl={to}&dt=t&q={HttpUtility.UrlEncode(input)}";
        //    var webClient = new WebClient
        //    {
        //        Encoding = System.Text.Encoding.UTF8,
        //    };
        //    var result = webClient.DownloadString(url);
        //    try
        //    {
        //        result = result.Substring(4, result.IndexOf("\"", 4, StringComparison.Ordinal) - 4);
        //        return result;
        //    }
        //    catch
        //    {
        //        return "Error";
        //    }

        //}

        //private List<string> TranslateGoogleApi (List<string> texts,
        //    string fromLanguage,
        //    string targetLanguage)
        //{
        //    // De: https://cloud.google.com/dotnet/docs/reference/Google.Cloud.Translate.V3/latest/index
        //    var translationServiceClient = TranslationServiceClient.Create();
        //    var request = new TranslateTextRequest
        //    {
        //        Contents = 
        //        { 
        //            //a traduir
        //            texts
        //        },
        //        SourceLanguageCode = fromLanguage,
        //        TargetLanguageCode = targetLanguage,
        //        ParentAsLocationName = new LocationName("subtitlestranslator-331117", "global"),
        //    };
        //    TranslateTextResponse response = translationServiceClient.TranslateText(request);
        //    //Display the translation for each input text provided
        //    //foreach (Translation translation in response.Translations)
        //    //{
        //    //    Console.WriteLine($"Detected language: {translation.DetectedLanguageCode}");
        //    //Console.WriteLine($"Translated text: {translation.TranslatedText}");
        //    //} 
        //    return response.Translations.Select(x => x.TranslatedText).ToList();
        //}
    }
    class Translation
    {
        public string translatedText { get; set; }
        public string detectedSourceLanguage { get; set; }
    }
    class Data
    {
        public List<Translation> translations { get; set; }
    }
    class Root
    {
        public Data data { get; set; }
    }
    class JsonKey
    {
        public string Key { get; set; }
    }
}
