﻿using SubtitlesTranslator.App.WPF.Lib.UI;
using SubtitlesTranslator.App.WPF.Lib.Models;
using System.Collections.ObjectModel;
using System.Windows.Documents;
using System.Windows.Input;
using System.Linq;
using System.Windows;
using System.Collections.Generic;
using SubtitlesTranslator.Lib.Services;
using System;
using System.IO;

namespace SubtitlesTranslator.App.WPF.ViewModels
{
    public class EditViewModel : ViewModelBase
    {
        #region Services

        //métode pel service
        //Injeccio de property
        public ITranslatorService TranslatorService { get; set; }
        #endregion


        #region Commands

        public ICommand ExportCommand { get; set; }
        public ICommand SelectEnglishSubtitlesCommand { get; set; }
        public ICommand SelectSpanishSubtitlesCommand { get; set; }
        #endregion


        #region View Actions

        //notifica quan canviem el Language
        public Action<string>  NotifyCurrentLanguageChanged { get; set; }
        #endregion

        public string CurrentFileName { get; set; }

        //Per quan editem l'idioma de sortida, fem un Dictionary
        private Dictionary<string, ObservableCollection<SubtitleLine>> LinesByLanguage { get; set; }

       
        //Observable = colecció (millor que el List) per treballar amb Bindings
        public ObservableCollection<SubtitleLine> CurrentLines
        {
            get
            {
                return _currentlines;
            }
            set
            {
                _currentlines = value;
                NotifyPropertyChange();
            }
        }
        private ObservableCollection<SubtitleLine> _currentlines = new ObservableCollection<SubtitleLine>();

        //property = idioma actual
        public string CurrentLanguage
        { 
            //{ get => currentLanguage; set => currentLanguage = value; }
            get
                {
                    return _currentLanguage;
                }
            set
                {
                    _currentLanguage = value;
                    NotifyPropertyChange();

                    if(NotifyCurrentLanguageChanged != null)
                        NotifyCurrentLanguageChanged(_currentLanguage); 
                }
        }
        private string _currentLanguage;

        public string ImportedLanguage { get; set; }

        //passem el service com a param
        public EditViewModel(ITranslatorService translatorService)
        {
            TranslatorService = translatorService;
            //Creem el Dictionary + linees
            LinesByLanguage = new Dictionary<string, ObservableCollection<SubtitleLine>>();

            ExportCommand = new ModelCommand(x => Export());
            SelectEnglishSubtitlesCommand = new ModelCommand(x => SelectEnglishSubtitles());
            SelectSpanishSubtitlesCommand = new ModelCommand(x => SelectSpanishSubtitles());
        }

        //per passar les linees i el llenguatge escollit
        public void SelectImportLines(List<SubtitleLine> lines, string importLanguage, string fileName) 
        {
            CurrentFileName = fileName;

            if(LinesByLanguage.Count>0)
            {
                //si existeix, esborra el que hi havia abans
                LinesByLanguage = new Dictionary<string, ObservableCollection<SubtitleLine>>();
                CurrentLines = new ObservableCollection<SubtitleLine>();
            }

            //per si cliquen diverses vegades l'Import, capturem la primera only
            if (!LinesByLanguage.ContainsKey(importLanguage))
            {
                LinesByLanguage.Add(importLanguage, new ObservableCollection<SubtitleLine>(lines));
                //traducció
                TranslateToOtherLangues(lines, importLanguage);
            }
            else
            {
                //Exception? error = Exception;
                //MessageBox.Show(error.Message);
                
            }
        }


        public void Export()
        {
            //var fileOriginal = Path.GetFileName(CurrentFileName).Replace(".vtt", string.Empty);
            var fileOriginal = Path.GetFileName(CurrentFileName);
            var path = Path.GetDirectoryName(CurrentFileName);

            var lines = new List<string>();
            lines.Add("WEBVTT");
            lines.Add(string.Empty);

            foreach(var line in CurrentLines)
            {
                lines.Add(line.LineNumber.ToString());
                lines.Add(line.Period.ToString());
                lines.Add(line.Text.ToString());
                lines.Add(string.Empty);
            }

            var newFileName = path + "\\" + CurrentLanguage + "_" + fileOriginal;
            File.WriteAllLines(newFileName, lines.ToArray());

            MessageBox.Show ("Export OK  " + newFileName);
        }

        public void TranslateToOtherLangues (List<SubtitleLine> lines, string importLanguage)
        {
            switch(importLanguage)
            {
                case "ESP":
                    TranslateToLangue(lines, importLanguage, "EN");
                    break;
                case "EN":
                    TranslateToLangue(lines, importLanguage, "ESP");
                    break;
                default:
                    MessageBox.Show($"Language {importLanguage} not supported");
                    break;
            }
        }

        //from ESP => afegir traducció al Dictionary ENG
        private void TranslateToLangue (List<SubtitleLine> lines, string from, string to)
        {
            LinesByLanguage.Add(to, new ObservableCollection<SubtitleLine>());
            CurrentLines = LinesByLanguage[to];
            //crida al service
            TranslatorService.Translate(from, to, lines, CurrentLines);
            
            CurrentLanguage = to;
        }

        public void SelectEnglishSubtitles()
        {
            SelectSubtitles("EN");
        }
        public void SelectSpanishSubtitles()
        {
            SelectSubtitles("ESP");
        }
        private void SelectSubtitles(string language)
        {
            //per canviar colors dels botons
            if(language != ImportedLanguage)
            {
                CurrentLines = LinesByLanguage[language];
                CurrentLanguage = language;
            }
          
        }
    }
}
