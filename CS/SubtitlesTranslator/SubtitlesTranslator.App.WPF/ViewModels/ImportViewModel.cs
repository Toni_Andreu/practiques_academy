﻿using SubtitlesTranslator.App.WPF.Lib;
using SubtitlesTranslator.App.WPF.Lib.Models;
using SubtitlesTranslator.App.WPF.Lib.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Input;

namespace SubtitlesTranslator.App.WPF.ViewModels
{
    public class ImportViewModel : ViewModelBase
    {
        //llegir Files = func delegat
        public Func<List<string>> GetSubtitlesFilesAction { get; set; }

        //llegir txt: param in, param out (línees de txt)
        public Func<string, List<string>> GetFileTextLinesAction { get; set; }

        //un altre Delegat= acció que passa una llista + Language
        public Action<List<SubtitleLine>, string, string> SelectSubtitlesForEditAction {  get; set;}

        public List<string> FilesNames 
        {
            get
            {
                return _filesName;
            }
            set
            { 
                _filesName = value;
                NotifyPropertyChange(); 
            }
        }
        private List<string> _filesName = new List<string>();

        public IEnumerable<string> LanguageTypes
        {
            get
            {
                //llista del Diccionari
                return LanguageOptions.Types.Keys;
            }
        }
    
        public string SelectedLanguage
        {
            get
            {
                return _selectedLanguage;
            }
            set
            {
                _selectedLanguage = value;
                NotifyPropertyChange();
            }
        }
        private string _selectedLanguage;

        public string SelectedFile
        {
            get
            {
                return _selectedFile;
            }
            set
            {
                
                if (_selectedFile != value)
                {
                    _selectedFile = value;
                    SelectFile(value);
                }
                //per rebre notificació quan canviï el fitxer
                NotifyPropertyChange();
            }
        }
        private string _selectedFile;

        public ImportViewModel()
        {
            //Language per defecte
            SelectedLanguage = "ESP";
            //mapegem
            ImportCommand = new ModelCommand(x => Import());

        }

        //control del ViewMode per que llegeixi una carpeta
        private void Import()
        {
            if (GetSubtitlesFilesAction != null)
            {
                //ordenats
                FilesNames = GetSubtitlesFilesAction().OrderBy(x => x).ToList(); 
            }

        }

        private void SelectFile(string fileName)
        {
            //treiem el txt
            
            if(GetFileTextLinesAction != null)
            {
                var textLines = GetFileTextLinesAction(fileName);
                try
                {
                    var subtitleslines = SubtitleLine.GetTextFromLines(textLines);
                    if ( SelectSubtitlesForEditAction != null )
                        //linees + idioma seleccionat
                        SelectSubtitlesForEditAction(subtitleslines, SelectedLanguage, fileName);
                }
                catch (ArgumentException ex)
                {
                    MessageBox.Show(ex.Message);
                }  
            }
        }
        public ICommand ImportCommand { get; set; }



    }
}
