﻿using SubtitlesTranslator.App.WPF.Lib.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace SubtitlesTranslator.App.WPF
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

            //ImportView parla amb el ViewModels i li passa linees + idioma
            ImportView.ViewModel.SelectSubtitlesForEditAction = (lines, language, fileName) =>
            {
                EditView.ViewModel.SelectImportLines(lines, language, fileName);
            };
        }
    }
}
