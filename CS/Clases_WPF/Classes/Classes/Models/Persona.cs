﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Classes
{
    public class Persona
    {
        // fields = _field
        private string _name;

        public string Name
        {
            get
            {
                return _name;
            }
            // no deixem escriure = RESTRICCIÓ
            //set
            //{
            //    _name = value;
            //}
            // El fem privat per ficar altres restriccions = Propertie!
            private set
            {
                if (value.Length == 0)
                {
                    throw new ArgumentException("El valor de nom no pot estar buid.");
                }
                else if (Char.IsLower(value[0]))
                {
                    //primera lletra SEMPRE en maiúscules!
                    _name = value[0].ToString().ToUpper() + value.Substring(1, value.Length - 1);
                }
                else
                {
                    _name = value;
                }

            }

        }

        // manera simple d'escriure set/Get sense restriccions
        //public string Name { get; set; }

        public int Year { get; set; }
        public Persona(string name)
        //només deixem crear des de la funció
        {
            _name = name;
        }

        //altres constructors => altres parametres  = Overload!
        public Persona(string name, int year)
        { }
        public Persona(string name, string position)
        { }

        public virtual void Asseures()
        {
            /// virtual = la classe FILLA ho decideix
            Console.WriteLine("M'assec on jo vulguic!");
        }
    }
}
