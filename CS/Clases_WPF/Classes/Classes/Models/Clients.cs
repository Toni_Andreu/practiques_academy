﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Classes
{
    public class Client : Persona
    {
        /// CLient HERETA de Persona
        /// 

        public Client(string name, int year = 2021)
            : base(name, year)
        {
            //alta Client + order
            this.Order(1, "olives", false);
        }
        public void Order(int waiterId, string plato)
        {
            Console.WriteLine($"El client {Name} li demana al cambrer {waiterId} un plat de {plato}");
        }
        public void Order(int waiterId, string plato, bool tengoPrisa = false)
        {
            Console.WriteLine($"El client {Name} li demana al cambrer {waiterId} un plat de {plato}");
        }

        public override void Asseures()
        {
            //base.Asseures()
            //OVERRRIDE
            Console.WriteLine("M'assec en una taula maca!");
        }
    }


}
